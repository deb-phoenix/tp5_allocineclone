<p align="center">
<img width="40%" src="allocineclone.png">
</p>

# WELCOME
AlloCinéClone, fonctionne avec l'API de The Movie DataBase.
Proposé en version JS et en version React App.
<br/>

## BRIEF
Créer un site internet qui permet de rechercher n’importe quel film et connaître toutes les infos (titre, durée, réalisateur, etc).
<br/>

## INSTRUCTIONS
Utilisation l’API de The Movie DB.

Le site doit proposer un champ de recherche qui permet de taper le titre d’un film et de faire une recherche sur la base.

Chaque élément des résultats de recherche doit être cliquable et amène vers la page du film. Dans cette page, on affiche toutes les informations utiles pour le film.

Aucune maquette graphique définie. Mettre l’accent sur l’UI/UX, l’accessibilité et le SEO.<br/>

## TECHNO-TAGS
- HTML5
- CSS3
- SASS
- Bootstrap4
- JS ES6+
- Json
- NPM/ ESLint/ StyleLint
- React
- UX/UI
- SEO

## AUTEURE
- Déb Phoenix ~ *good vibes only*
- Proudly student powered by: http://simplon.co

## COPYRIGHT
- [The Movie DataBase](https://www.themoviedb.org/)
- [Allociné](https://allocine.fr)
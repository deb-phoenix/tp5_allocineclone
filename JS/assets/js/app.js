// valeur de départ
const INITIAL_SEARCH_VALUE = 'alien';

// selection des élèments
const searchInput = document.querySelector('#inputSearch');
const searchButton = document.querySelector('#btnSearch');
const movieSearchable = document.querySelector('#searchMovie');
const moviesContainer = document.querySelector('#movies-container');
// const imgElement = document.querySelector('img'); 

// imgElement.onclick = function(){
// }


function requestMovies(url, onComplete, onError) {
    // recupère les données de TMDB depuis le champ de recherche
    fetch(url)
        .then((res) => res.json())
        .then(onComplete)
        .catch(onError);
}

    // recup les données de TMDB depuis une le champ de recherche
    // fetch(newUrl)
    //     .then((res) => res.json())
    //     .then((data) => {
    //         console.log('Données: ', data);
    //     })
    //     .catch((error) => {
    //         console.log('Erreur: ', error);
    //     });



// fonction qui permet la recherche sur TMDB
function searchMovie(value) {
    const url = generateMovieDBUrl('/search/movie') + '&query=' + value + '&language=fr';
    requestMovies(url, renderSearchMovies, handleGeneralError);
}

// renvoi à la constante INITIAL_SEARCH_VALUE
// qui indique une valeur par défaut
searchMovie(INITIAL_SEARCH_VALUE);

// fonction qui crée l'élèment div avec ses attributs
// qui va contenir l'affiche du film
function createImageContainer(imageUrl, id) {
    const divImage = document.createElement('div');
    divImage.setAttribute('class', 'imageContainer');
    divImage.setAttribute('data-id', id);

    // template string de la balise img
    // récupère l'url de l'affiche du film en fonction de l'id du film
    const movieElement = `
        <img src="${imageUrl}" alt="affiche du film" data-movie-id="${id}">
    `;
    divImage.innerHTML = movieElement;

    return divImage;
}

// fonction qui vide le champ de recherche
function resetInput() {
    searchInput.value = '';
}


function handleGeneralError(error) {
    log('Erreur: ', error);
}

// fonction qui crée le titre des sections
function createSectionHeader(title) {
    const header = document.createElement('h2');
    header.innerHTML = title;
    return header;
}


function renderMovies(data) {
    const moviesBlock = generateMoviesBlock(data);
    const header = createSectionHeader(this.title);
    moviesBlock.insertBefore(header, moviesBlock.firstChild);
    moviesContainer.appendChild(moviesBlock);
}


function renderSearchMovies(data) {
    movieSearchable.innerHTML = '';
    const moviesBlock = generateMoviesBlock(data);
    movieSearchable.appendChild(moviesBlock);
}

function generateMoviesBlock(data) {
    const movies = data.results;
    const section = document.createElement('section');
    section.setAttribute('class', 'section');

    for (let i = 0; i < movies.length; i++) {
        const { poster_path, id } = movies[i];

        if (poster_path) {
            const imageUrl = MOVIE_DB_IMAGE_ENDPOINT + poster_path;
    
            const imageContainer = createImageContainer(imageUrl, id);
            section.appendChild(imageContainer);
        }
    }

    const movieSectionAndContent = createMovieContainer(section);
    return movieSectionAndContent;
}


// crée la section avant l'élèment movie
function createMovieContainer(section) {
    const movieElement = document.createElement('div');
    movieElement.setAttribute('class', 'movie');
    movieElement.insertBefore(section, movieElement.firstChild);
    return movieElement;
}


searchButton.onclick = function (event) {
    // empêche le raffraichissement au submit
    event.preventDefault();
    const value = searchInput.value

   if (value) {
    searchMovie(value);
   }
    resetInput();
}



searchUpcomingMovies();
getTopRatedMovies();
searchPopularMovie();

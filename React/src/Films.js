
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { PureComponent} from 'react'

/* import des composants bootstrap */
import { Container, Row, Col, Button, Card} from 'react-bootstrap';

/* eslint-disable react/prop-types */
class Films extends PureComponent {
  viewMovie() {
    // console.log("voir le film")
    // console.log(this.props.movie.title)
    const url = "https://www.themoviedb.org/movie/" + this.props.movie.id
    window.location.href = url
  }

   /* Les données descendent à travers l'arborescence de composants. Mécanisme clé: props.
  Les composants parents et enfants: pas identiques à leur homologue DOM, en React on qualifie de composant enfant tout composant défini dans le render du composant parent, quel que soit son niveau de profondeur */

  /* eslint-disable react/prop-types */
  render() {
    return<Container fluid>
      <Row>
        <Col className="py-0 my-3">
          <Card border="warning" key={this.props.movie.id}>
            <Card.Header className="border-bottom-0 cardTitle">{this.props.movie.title}</Card.Header>
              <Card.Body>
                <Card.Text>
                <img alt="poster" width="120" src={this.props.movie.poster_src} className="rounded-lg posterAlign"/> {this.props.movie.overview}
                </Card.Text>
              </Card.Body>
          </Card>

          <Button variant="outline-warning" onClick={this.viewMovie.bind(this)} className="my-2">voir la fiche</Button>{' '}
        </Col>
      </Row>
  </Container>
  }
}

export default Films
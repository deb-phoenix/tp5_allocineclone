
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React, { PureComponent } from 'react';
import './App.css';
import Films from './Films.js'
import Keys from './Key.js'

// import des composants bootstrap
import { Container, Row, Col, Form } from 'react-bootstrap';
// import de jquery
import $ from 'jquery'
// import de l'effet typewriter
import Typewriter from 'typewriter-effect';

// react permet d'utiliser des classes ES6 pour définir les composants
// utilisation du composant React. PureComponent plutot que React. Component pour raffraichir les modifications
//App est une classe de composant React ou type de composant React
class App extends PureComponent {

  // la méthode constructor est une méthode spéciale qui permet de créer et d'initialiser les objet créés avec une classe
  //un composant utilise des paramètres appelés props pour propriétés
  constructor(props) {
    
    // Le constructeur ainsi déclaré peut utiliser le mot-clé super afin d'appeler le constructeur de la classe parente
    super (props)
    this.state = {}


    // console.log("Ceci est un test")

    // const movies = [
    //   {id: 0, poster_src: "https://image.tmdb.org/t/p/w188_and_h282_bestv2/hSfuKPtyEryeFzapZ8UgZd4aESu.jpg", title: "Avengers: Infinity War", overview: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters."},
    //   {id: 1, poster_src: "https://image.tmdb.org/t/p/w188_and_h282_bestv2/ntpxTsTg3BsIRkMkNLffNjwftUf.jpg", title: "The Avengers", overview: "Ceci est le résumé du film."}
    // ]


    // let movieRows = []
    // movies.forEach((movie) => {
    //   console.log(movie.title)
    //   const movieRow = <Films movie={movie} />
    //   movieRows.push(movieRow)
    // })

    // this.state = {rows: movieRows}
  
    this.performSearch("alien")
  }
    // méthode qui permet la recherche dans TMDB
    performSearch(searchTerm) {
      console.log("Lancer une recherche avec MDB")
      const urlString = "https://api.themoviedb.org/3/search/movie?&api_key=af431e996ed24cbd19191ad3e0084349&query=" + searchTerm + "&language=fr"
      $.ajax({
        url : urlString,
        success: (searchResults) => {
          console.log("Recherche reussie!")
          // console.log(searchResults)
          const results = searchResults.results
          // console.log(results[0])

          let movieRows = []

          // boucle qui utilise la fonction movie sur chaque élèment du tableau
          results.forEach((movie) => {
            movie.poster_src = "https://image.tmdb.org/t/p/w185" + movie.poster_path
            // console.log(movie.poster_path)
            const movieRow = <Films key={movie.id} movie={movie}/>
            movieRows.push(movieRow)
          })

          this.setState({rows : movieRows})
          /* extension syntaxique de js,
          React m'oblige pas à utiliser jsx,
          mais bcp de gens y trouvent une aide visuelle, accolades.
          Ceci est une expression jsx. Les expressions jsx après la compilation,
          des simples appels de fonctions js qui renvoient des objets js */
        },

        error: (xhr, status, err) => {
        console.log("Erreur de récupération des données")
        }
      })
   }

   searchChangeHandler(event){
     console.log(event.target.value)
     const boundObject = this
     const searchTerm = event.target.value
     boundObject.performSearch(searchTerm)
   }

    // CONTENU PRINCIPAL // ------------------------------

    //le composant accepte des paramètres (props) ET renvoie via sa méthode render une arborescence de vues à afficher
    // render renvoie une description de ce qui va être vu à l’écran
    render() { 
    return (
      <Container fluid >
        <Row>
          <Col className="titrePrincipal">
            <table>
              <tbody>
                <tr>
                  <td>
                    <img alt="" role="presentation" width="30" src="../allocine.png"/>
                  </td>
                  <td width="8"/>
                  <td>
                    <h1><Typewriter
                        options={{
                        strings: ['AlloCinéClone', 'Tout sur le cinéma'],
                        autoStart: true,
                        loop: true,
                        }}/>
                    </h1>
                  </td>
                </tr>
              </tbody>
            </table>

            <Form.Control
            className="border border-warning"
            type="text"
            placeholder="saisir le nom du film ici"
            onChange={this.searchChangeHandler.bind(this)} />
          </Col>
        </Row>

        {this.state.rows}

      </Container>
    );
  }
}


export default App;
